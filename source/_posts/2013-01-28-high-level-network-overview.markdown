---
layout: post
title: "High-Level Network Overview"
date: 2013-01-28 09:28
comments: true
categories: Networking Purchasing
---

To start this thing off. I'm going to describe the high-level network overview in the PN sector of Federation space. There won't be a lot of technical documentation of low-level configuration here, but more of an overview of what kinds of things you may need to purchase, and how you should be thinking about future longevity.

<!-- more -->

## Astaro@Home / Sophos UTM Home Edition

For our main router / UTM solution, we've decided to use Astaro. This product is [completely free to use at home](http://www.sophos.com/en-us/products/free-tools/sophos-utm-home-edition.aspx) for up to 50 IP addresses. It is running on a newly-built computer, utilizing an AMD APU and 4GBs of RAM.
Nothing has sped up my network more than the Astaro. It really is the center point of the UFS network. From DHCP to IPsec, this one piece of software does it all. Many tutorials on UFS will be centered around Astaro as a central solution.

## Cisco WAP and POE Injector

For our wireless access point, we decided early on that home WAP devices were simply too underpowered for the amount of traffic we were generating. Cisco devices are difficult to configure (unless you've been around them a while), but absolutely rock-steady once they are configured and on the wall. We went with the [Cisco AIR-AP1041N-A-K9 Aironet 802.11g/n Fixed Autonomous Access Point](http://www.amazon.com/gp/product/B004PHVQFW/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B004PHVQFW&linkCode=as2&tag=thbloftowe-20), which was a bit expensive for a home network, but completely worth it. It handles every bit of traffic I can throw at it, extremely fast, and completely stable. It does have a web interface for configuration, but it is nowhere near user-friendly. Make sure you have some familiarity with Cisco devices before you plug this one in, and speaking of plugging in: You'll need one of these [TRENDnet Gigabit Power over Ethernet (PoE) Injector TPE-111GI](http://www.amazon.com/gp/product/B00180ID7I/ref=as_li_ss_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=B00180ID7I&linkCode=as2&tag=thbloftowe-20), a POE injector. Instead of the switch providing POE, you have a pass-through device that supplies power from a normal wall outlet. I've been using this particular injector for almost a year, constantly, without any trouble.

## DLink 24-port Gigabit Switch

Tying the network together is a 24-port gigabit switch. A good switch is extremely important to have in a fast network environment. If your switch is 100 megabit, and everything else is gigabit, you're losing 90% of your speed because you wanted to save some money. I decided not to jump for the POE version, for cost-saving reasons. My place is small enough that I only need one access point, and no POE devices other than that.

## Box of 1000 Feet of Cat 5e

On this, your milage may vary. You want to use at least Cat 5e in your network, so you have the option of expanding to more POE devices, should you need them. Cabling is one of the things you should over-do initially, because it is much harder to change. Instead of dropping a new box in place, you instead have to re-run all of your cable. Much harder to change, much more time-consuming. I would suggest buying double jack boxes and running two lines to each drop. Too much connectivity is rarely a problem, and if you're running cable anyway, why not? Before you start wiring, you'll need some RJ45 plugs, keystones, a basic punchdown tool, and a nice RJ45 compatible crimping tool. I can't stress spending the extra money on a nice crimper enough. You'll spend much less time troubleshooting and re-doing wiring if you do it correctly the first time. A good set of tools will help you do that.

## Why not 10 Gig?

10 Gig is nice, but the price hasn't quite reached the point where its economically feasible to outfit an entire home network with it. Maybe in a few years it won't be prohibitively expensive to outfit everything, including your servers, network devices, and wiring with 10G compatible interfaces, but today, that is not the case. When setting up your ultra-professional home network, approach the problem like you would when building a computer. Sure, you can spend $4000 on a computer, but if you wait only a year, that same machine is about $1200. When planning your network, try to hit the magical place between lifetime and value.

## Overview

There isn't a lot of technical information in this article. It is only designed to help you visualize your future network plans. In the next segment, I'll introduce you to Astaro configuration and deployment.