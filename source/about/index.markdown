---
layout: page
title: "About"
date: 2013-01-23 05:40
comments: false
sharing: true
footer: true
---

## What is this place?

The United Federation of Servers is a collection of ideas around open technologies and empowering people with the best tech available.

## Why should I care?

I think I've built some very cool things with tech, and I think others could benefit from it. So I'm open-sourcing the federation projects, big and small, right here.

## What should I expect?

Posts on `Astaro Security Gateway`, `Various VPN Technologies`, `IPsec`, `OpenStack`, `Wireless Access Point Configuration`, and `Networking`.
The general idea behind this blog is to help you create the very best network you can with free software (and sometimes cheap hardware, too). I started this a while ago with [Server Bits](http://www.samurailink3.com) on my personal blog, but I've since changed just about all of the technology I'm using to accomplish my goals, so I need a new place to write about Federation ideals.

## Why 'Federation' ?

A few years ago, while setting up a home network, a few friends and I realized that we were duplicating content locally, while each of us had pretty nice internet connections. We proceeded to link our servers via SSH and created a mini-darknet. The primary ideal behind Federation servers is that each should be independent, while possessing the ability to link together in a sort of WAN formation. To share data, resources, or replication services. Learning is the highest priority, and most of these posts will refect that.
